// Hong Kheatbung, [05.08.20 13:44]
import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import Hidden from "@material-ui/core/Hidden";
// @material-ui/icons
import Menu from "@material-ui/icons/Menu";
// core components
import AdminNavbarLinks from "./AdminNavbarLinks.js";
import RTLNavbarLinks from "./RTLNavbarLinks.js";
import Button from "components/CustomButtons/Button.js";

import styles from "assets/jss/material-dashboard-react/components/headerStyle.js";
// custom style
import "assets/css/Style.css";
import Box from "@material-ui/core/Box";
import 'assets/css/AdminNavStyle.css'
const useStyles = makeStyles(styles);

export default function Header(props) {
  const classes = useStyles();
  function makeBrand() {
    var name;
    props.routes.map((prop) => {
      if (window.location.href.indexOf(prop.layout + prop.path) !== -1) {
        name = props.rtlActive ? prop.rtlName : prop.name;
      }
      return null;
	});
    return name;
  }
  const { color } = props;
  const appBarClasses = classNames({
    [" " + classes[color]]: color,
  });
  return (
    <AppBar className={classes.appBar + appBarClasses}>
      <Toolbar className={classes.container}>
        <div className={classes.flex}>
          {/* Here we create navbar brand, based on route name */}
          <Box display="flex" style={{marginLeft:"15px",borderBottom:"3px solid #4C68FF"}}>
            <Box flexGrow={1}>	
              <Button
                color="transparent"
                className={classes.title}
                style={{
                  padding: "0",
                  margin: "0",
                }}
              >
                <div
                  style={{
                    backgroundColor: "#4C68FF",
                    width: "40px",
                    height: "50px",
                  }}
                ></div>
                <div
                  style={{
                    fontSize: "30px",
                    fontWeight: "bold",
                    paddingBottom: "0",
                    marginLeft: "10px",
                  }}
                >
                  {makeBrand()!=null?makeBrand():"Reports"}
                </div>
              </Button>
            </Box>
            <Box
              style={{
                padding: "0",
                margin: "0",
              }}
            >
              <div
                style={{
             
                  padding: "0",
                  margin: "0",
                }}
                className="hide"
              >
                {AdminNavbarLinks()}
              </div>
            </Box>
          </Box>
          {/* <div
            style={{
              borderBottom: "3px solid #4C68FF",
              marginLeft: "1.2%",
              marginRight: "1.2%",
              paddingTop: "0",
              marginTop: "0",
            }}
          ></div> */}
        </div>
        <Hidden mdUp implementation="css">
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={props.handleDrawerToggle}
          >
            <Menu />
          </IconButton>
        </Hidden>
      </Toolbar>
    </AppBar>
  );
}

Header.propTypes = {
  color: PropTypes.oneOf(["primary", "info", "success", "warning", "danger"]),
  rtlActive: PropTypes.bool,
  handleDrawerToggle: PropTypes.func,
  routes: PropTypes.arrayOf(PropTypes.object),
};