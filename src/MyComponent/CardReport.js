import React, { Component } from 'react';
import Box from '@material-ui/core/Box';
import TimePng from 'assets/img/Time.png';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Panha from 'assets/img/Panha.jpg';
import { makeStyles, withStyles } from '@material-ui/core/styles';
const cardStyle = makeStyles({
	root: {
		minWidth: 275,
	},
	bullet: {
		display: 'inline-block',
		margin: '0 2px',
		transform: 'scale(0.8)',
	},
	title: {
		fontSize: 14,
	},
	pos: {
		marginBottom: 12,
	},
});
export default class CardReport extends Component {
	render() {
		return (
			<Card className={cardStyle.root} style={{marginBottom:"15px"}}>
				<CardContent>
					<Box display="flex" justifyContent="flex-start" m={1} p={1} style={{ margin: '0', padding: '0' }}>
						<Box>
							<img src={Panha} className="user" />
						</Box>
						<Box style={{ marginLeft: '15px' }}>
							<div className="content">
								<h6 className="nameuser">Long Panh</h6>
								<p>
									I attended one interview for test automation using selenium. In my interview , one
									ofthe questionsis how to handle or find webelement when there are no locators
									available to same{' '}
								</p>
								<Box
									display="flex"
									justifyContent="flex-start"
									m={1}
									p={1}
									style={{ margin: '0', padding: '0' }}
								>
									<Box p={1} flexGrow={1}>
										<p className="nameuser">
											Report Type:<span> អាសអាភាស់</span>
										</p>
									</Box>
									<Box flexGrow={5}>
										<Box
											display="flex"
											justifyContent="flex-start"
											m={1}
											p={1}
											style={{ margin: '0', padding: '0' }}
										>
											<Box p={1}>
												<img src={TimePng} className="user" />
											</Box>
											<Box p={1} flexGrow={15}>
												<h6 className="nameuser">
													Report date:<span>មករា​ ១ ២០២០</span>
												</h6>
											</Box>
										</Box>
									</Box>
								</Box>
							</div>
						</Box>
					</Box>
				</CardContent>
			</Card>
		);
	}
}
