import React from 'react';
// react plugin for creating charts
import ChartistGraph from 'react-chartist';
// @material-ui/core
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@material-ui/core/Icon';
// @material-ui/icons
import Store from '@material-ui/icons/Store';
import Warning from '@material-ui/icons/Warning';
import DateRange from '@material-ui/icons/DateRange';
import LocalOffer from '@material-ui/icons/LocalOffer';
import Update from '@material-ui/icons/Update';
import ArrowUpward from '@material-ui/icons/ArrowUpward';
import AccessTime from '@material-ui/icons/AccessTime';
import Accessibility from '@material-ui/icons/Accessibility';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
// import BugReport from "@material-ui/icons/BugReport";
// core components
import GridItem from 'components/Grid/GridItem.js';
import GridContainer from 'components/Grid/GridContainer.js';
// import Table from "components/Table/Table.js";
// import Tasks from "components/Tasks/Tasks.js";
// import CustomTabs from "components/CustomTabs/CustomTabs.js";
import Danger from 'components/Typography/Danger.js';
import Card from 'components/Card/Card.js';
import CardHeader from 'components/Card/CardHeader.js';
import CardIcon from 'components/Card/CardIcon.js';
import CardBody from 'components/Card/CardBody.js';
import CardFooter from 'components/Card/CardFooter.js';
import user from '../../assets/img/user.png';
import ReportIcon from '@material-ui/icons/Report';
import ContactSupportIcon from '@material-ui/icons/ContactSupport';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
// import { bugs, website, server } from "variables/general.js";

import { dailySalesChart, emailsSubscriptionChart, completedTasksChart } from 'variables/charts.js';

import styles from 'assets/jss/material-dashboard-react/views/dashboardStyle.js';
import { Link } from 'react-router-dom';
const useStyles = makeStyles(styles);

export default function Dashboard() {
	const classes = useStyles();
	return (
		<div>
			<GridContainer>
				<GridItem xs={12} sm={6} md={3}>
					<Card>
						<CardHeader color="warning" stats icon>
							<CardIcon color="warning">
								<AccountCircleOutlinedIcon />
							</CardIcon>
							<h2 className={classes.cardCategory}>Users</h2>
							<h4 className={classes.cardTitle}>100</h4>
						</CardHeader>
						<CardFooter stats>
							<div style={{ margin: '0 auto' }} className={classes.stats}>
								{/* <a href="/admin/tableUser" >
									View Detail
								</a> */}
								<Link to="/admin/tableUser">View Detail</Link>
							</div>
						</CardFooter>
					</Card>
				</GridItem>
				<GridItem xs={12} sm={6} md={3}>
					<Card>
						<CardHeader color="success" stats icon>
							<CardIcon color="success">
								<Icon>content_paste</Icon>
							</CardIcon>
							<h2 className={classes.cardCategory}>Categories</h2>
							<h4 className={classes.cardTitle}>100</h4>
						</CardHeader>
						<CardFooter stats>
							<div style={{ margin: '0 auto' }} className={classes.stats}>
								<Link to="/admin/category">View Detail</Link>
							</div>
						</CardFooter>
					</Card>
				</GridItem>
				<GridItem xs={12} sm={6} md={3}>
					<Card>
						<CardHeader color="danger" stats icon>
							<CardIcon color="danger">
								<ContactSupportIcon />
							</CardIcon>
							<h2 className={classes.cardCategory}>Questions</h2>
							<h4 className={classes.cardTitle}>100</h4>
						</CardHeader>
						<CardFooter stats>
							<div style={{ margin: '0 auto' }} className={classes.stats}>
							<Link to="/admin/question">View Detail</Link>
							</div>
						</CardFooter>
					</Card>
				</GridItem>
				<GridItem xs={12} sm={6} md={3}>
					<Card>
						<CardHeader color="info" stats icon>
							<CardIcon color="info">
								<ReportIcon />
							</CardIcon>
							<h2 className={classes.cardCategory}>Reports</h2>
							<h4 className={classes.cardTitle}>100</h4>
						</CardHeader>
						<CardFooter stats>
							<div style={{ margin: '0 auto' }} className={classes.stats}>
							<Link to="/admin/reports">View Detail</Link>
							</div>
						</CardFooter>
					</Card>
				</GridItem>
			</GridContainer>
			<GridContainer>
				<GridItem xs={12} sm={12} md={12}>
					<Card chart>
						<CardHeader color="success">
							<ChartistGraph
								className="ct-chart ct-double-octave"
								data={dailySalesChart.data}
								type="Line"
								options={dailySalesChart.options}
								listener={dailySalesChart.animation}
							/>
						</CardHeader>
						<CardBody>
							<h4 className={classes.cardTitle}>Monthly User Register</h4>
							<p className={classes.cardCategory}>
								{/* <span className={classes.successText}>
                  <ArrowUpward className={classes.upArrowCardCategory} /> 55%
                </span>{" "}
                increase in today sales. */}
							</p>
						</CardBody>
						{/* <CardFooter chart>
              <div className={classes.stats}>
                <AccessTime /> updated 4 minutes ago
              </div>
            </CardFooter> */}
					</Card>
				</GridItem>
				{/* <GridItem xs={12} sm={12} md={4}>
          <Card chart>
            <CardHeader color="warning">
              <ChartistGraph
                className="ct-chart"
                data={emailsSubscriptionChart.data}
                type="Bar"
                options={emailsSubscriptionChart.options}
                responsiveOptions={emailsSubscriptionChart.responsiveOptions}
                listener={emailsSubscriptionChart.animation}
              />
            </CardHeader>
            <CardBody>
              <h4 className={classes.cardTitle}>Email Subscriptions</h4>
              <p className={classes.cardCategory}>Last Campaign Performance</p>
            </CardBody>
            <CardFooter chart>
              <div className={classes.stats}>
                <AccessTime /> campaign sent 2 days ago
              </div>
            </CardFooter>
          </Card>
        </GridItem> */}
				{/* <GridItem xs={12} sm={12} md={4}>
          <Card chart>
            <CardHeader color="danger">
              <ChartistGraph
                className="ct-chart"
                data={completedTasksChart.data}
                type="Line"
                options={completedTasksChart.options}
                listener={completedTasksChart.animation}
              />
            </CardHeader>
            <CardBody>
              <h4 className={classes.cardTitle}>Completed Tasks</h4>
              <p className={classes.cardCategory}>Last Campaign Performance</p>
            </CardBody>
            <CardFooter chart>
              <div className={classes.stats}>
                <AccessTime /> campaign sent 2 days ago
              </div>
            </CardFooter>
          </Card>
        </GridItem> */}
			</GridContainer>
		</div>
	);
}
