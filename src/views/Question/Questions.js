// import React, { useEffect } from 'react';
import React, { Component } from 'react';
// @material-ui/core components
// import { makeStyles } from "@material-ui/core/styles";
// table
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Axios from 'axios';
import Pagination from '@material-ui/lab/Pagination';
import 'assets/css/QuestionStyle.css'
// const useStyles = makeStyles(styles);
const styte=makeStyles({
	palette: {
		secondary: {
		  main: '#00c853',
		},
	  },
})
const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: '#4C68FF',
		color: theme.palette.common.white,
	},
	body: {
		fontSize: 14,
	},
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: theme.palette.action.hover,
		},
	},
}))(TableRow);

function createData(id, username, ask_date, title, view, status) {
	return { id, username, ask_date, title, view, status };
}

const rows = [
	createData(1, 'Panha', '20/1/2020', 'Opening modal on screen change ', 30, 'Active'),
	createData(2, 'Kok', '20/1/2020', 'Editing metadata of music file with dart modal on screen change ', 12, 'Active'),
	createData(3, 'Panha', '20/1/2020', 'Opening modal on screen change ', 30, 'Disable'),
	createData(4, 'Kok', '20/1/2020', 'Editing metadata of music file with dart modal on screen change ', 12, 'Active'),
	createData(5, 'Panha', '20/1/2020', 'Opening modal on screen change ', 30, 'Active'),
	createData(6, 'Kok', '20/1/2020', 'Editing metadata of music file with dart modal on screen change ', 12, 'Disable'),
];

const useStyles = makeStyles({
	table: {
		minWidth: 700,
	},
});
const page = makeStyles((theme) => ({
	root: {
		'& > *': {
			marginTop: theme.spacing(2),
		},
	},
}));
export default class Questions extends Component {
	constructor(props) {
		super(props);
		this.state = {
			classes: {},
			person: [],
		};
	}
	componentWillMount() {
		this.setState({ classes: useStyles });
		Axios.get(`https://jsonplaceholder.typicode.com/users`).then((res) => {
			const persons = res.data;
			this.setState({ person: persons });
		});
	}
	render() {
		console.log(this.state.person);
		return (
			<div>
				<TextField
					id="outlined-basic"
					label="Search"
					variant="outlined"
					size="small"
					style={{ marginBottom: '20px', backgroundColor: '#FFFFFF' }}
				/>
				<TableContainer component={Paper}>
					<Table className={this.state.classes.table} aria-label="customized table">
						<TableHead>
							<TableRow>
								<StyledTableCell>Id</StyledTableCell>
								<StyledTableCell align="center">Username</StyledTableCell>
								<StyledTableCell align="center">Ask Date</StyledTableCell>
								<StyledTableCell align="center">Title</StyledTableCell>
								<StyledTableCell align="center">View</StyledTableCell>
								<StyledTableCell align="center">Status</StyledTableCell>
								<StyledTableCell align="center">Action</StyledTableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{rows.map((row) => (
								<StyledTableRow key={row.id}>
									<StyledTableCell component="th" scope="row">
										{row.id}
									</StyledTableCell>
									<StyledTableCell align="center">{row.username}</StyledTableCell>
									<StyledTableCell align="center">{row.ask_date}</StyledTableCell>
									<StyledTableCell align="center">{row.title}</StyledTableCell>
									<StyledTableCell align="center">{row.view}</StyledTableCell>
									<StyledTableCell align="center">{row.status}</StyledTableCell>
									<StyledTableCell align="center">
										{row.status == 'Active' ? (
											<Button variant="contained" color="secondary">
												Disable
											</Button>
										) : (
											<Button variant="contained" style={{backgroundColor:"#00c853",color:"white"}}>
												Active
											</Button>
										)}
										<Button
											variant="contained"
											color="primary"
											style={{ marginLeft: '5px', backgroundColor: '#4C68FF' }}
											onClick={() => alert(row.id)}
											className="colorBtn"
										>
											View
										</Button>
									</StyledTableCell>
								</StyledTableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
				<div className={page.root} style={{ marginTop: '20px' }}>
					<Pagination
						count={10}
						shape="rounded"
						color="primary"
						style={{ width: '345px', margin: '0 auto' }}
					/>
				</div>
			</div>
		);
	}
}
