import React, { Component } from 'react';
import Box from '@material-ui/core/Box';
import TimePng from 'assets/img/Time.png';
import CardReport from 'MyComponent/CardReport';
import ViewPng from 'assets/img/view.png';
import Panha from 'assets/img/Panha.jpg';
import codePng from 'assets/img/code.PNG';
import Button from '@material-ui/core/Button';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import MorePng from 'assets/img/more.png';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Menu from '@material-ui/core/Menu';
const StyledMenu = withStyles({
	paper: {
		border: '1px solid #d3d4d5',
	},
})((props) => (
	<Menu
		elevation={0}
		getContentAnchorEl={null}
		anchorOrigin={{
			vertical: 'bottom',
			horizontal: 'center',
		}}
		transformOrigin={{
			vertical: 'top',
			horizontal: 'center',
		}}
		{...props}
	/>
));
export default class ReportAnswer extends Component {
	constructor(props) {
		super(props);
		this.state = {
			anchorEl: null,
		};
	}
	handleClick = (event) => {
		this.setState({ anchorEl: event.currentTarget });
	};
	handleClose = () => {
		this.setState({ anchorEl: null });
	};
	render() {
		return (
			<div>
				<Box display="flex" m={1} p={1} style={{ margin: '0', padding: '0' }}>
					<Box flexGrow={1}>
						{/* <h5 className="titleQu">Does JSON data mean dictionary in Flask?</h5> */}
					</Box>
					<Box>
						<Button
							aria-controls="customized-menu"
							aria-haspopup="true"
							onClick={this.handleClick.bind(this)}
						>
							<img src={MorePng} className="user" />
						</Button>

						<StyledMenu
							id="customized-menu"
							anchorEl={this.state.anchorEl}
							keepMounted
							open={Boolean(this.state.anchorEl)}
							onClose={this.handleClose.bind(this)}
						>
							<MenuList id="customized-menu">
								<MenuItem onClick={() => alert('hello')}>Delete</MenuItem>
							</MenuList>
						</StyledMenu>
					</Box>
				</Box>
				<p className="content">
					But, for those of us fortunate enough to have the opportunity to reflect, take stock at this midyear
					mark on just how transformative 2020 has been. COVID-19 has changed our health, jobs, relationships,
					and behaviors. In the outbreak, protests for racial justice have started to change some social ins
				</p>
				<div style={{ textAlign: 'center' }}>
					{/* <img src={codePng} /> */}
				</div>
				<div style={{ width: '100%', borderBottom: '2px solid black' }}>
					<Box display="flex" justifyContent="flex-start" m={1} p={1} style={{ margin: '0', padding: '0' }}>
						<Box p={1}>
							<img src={Panha} className="user" />
						</Box>
						<Box p={1} flexGrow={5}>
							<h6 className="nameuser">Long Panha</h6>
						</Box>
						<Box p={1}>
							<img src={ViewPng} className="user" />
						</Box>
						<Box p={1} flexGrow={5}>
							<h6 className="nameuser">៤៣​ មើល</h6>
						</Box>
						<Box p={1}>
							<img src={TimePng} className="user" />
						</Box>
						<Box p={1} flexGrow={15}>
							<h6 className="nameuser">មករា​ ១ ២០២០</h6>
						</Box>
					</Box>
				</div>
				<ul className="ulHeader">
					<li className="subHeader" style={{ width: '75px' }}>
						Reporter
					</li>
				</ul>
				<div>
					<CardReport />
					<CardReport />
					<CardReport />
				</div>
			</div>
		);
	}
}
