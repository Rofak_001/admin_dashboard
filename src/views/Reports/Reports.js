import React, { Component } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Axios from 'axios';
import Pagination from '@material-ui/lab/Pagination';
import { Link, Redirect } from 'react-router-dom';

const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: '#4C68FF',
		color: theme.palette.common.white,
	},
	body: {
		fontSize: 14,
	},
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: theme.palette.action.hover,
		},
	},
}))(TableRow);

function createData(username, question, answer) {
	return { username, question, answer };
}

const rows = [
	createData('Rofak', 3, 2),
	createData('Panha', 5, 15),
	createData('Nazimah', 1, 15),
	createData('Kheatbung', 100, 1),
	createData('Chantrea', 30, 30),
	createData('Leapheng', 60, 5),
];

const useStyles = makeStyles({
	table: {
		minWidth: 700,
	},
});
const page = makeStyles((theme) => ({
	root: {
		'& > *': {
			marginTop: theme.spacing(2),
		},
	},
}));

export default class Reports extends Component {
	constructor(props) {
		super(props);
		this.state = {
			classes: {},
			person: [],
		};
	}
	componentWillMount() {
		this.setState({ classes: useStyles });
		Axios.get(`https://jsonplaceholder.typicode.com/users`).then((res) => {
			const persons = res.data;
			this.setState({ person: persons });
		});
	}
	render() {
		return (
			<div>
				<TextField
					id="outlined-basic"
					label="Search"
					variant="outlined"
					size="small"
					style={{ marginBottom: '20px', backgroundColor: '#FFFFFF' }}
				/>
				<TableContainer component={Paper}>
					<Table className={this.state.classes.table} aria-label="customized table">
						<TableHead>
							<TableRow>
								<StyledTableCell align="center">Username</StyledTableCell>
								<StyledTableCell align="center">Question</StyledTableCell>
								<StyledTableCell align="center">Answer</StyledTableCell>
								<StyledTableCell align="center">Action</StyledTableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{rows.map((row) => (
								<StyledTableRow key={row.username}>
									<StyledTableCell align="center">{row.username}</StyledTableCell>
									<StyledTableCell align="center">{row.question}</StyledTableCell>
									<StyledTableCell align="center">{row.answer}</StyledTableCell>
									<StyledTableCell align="center">
										<Link to="/admin/report_detail">
											<Button
												// href="/admin/report_detail"
												variant="contained"
												color="primary"
												style={{ marginLeft: '5px', backgroundColor: '#4C68FF' }}
											>
												View
											</Button>
										</Link>
									</StyledTableCell>
								</StyledTableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
				<div className={page.root} style={{ marginTop: '20px' }}>
					<Pagination
						count={10}
						shape="rounded"
						color="primary"
						style={{ width: '345px', margin: '0 auto' }}
					/>
				</div>
			</div>
		);
	}
}
