import React, { Component } from 'react';
import '../../assets/css/Style.css';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Axios from 'axios';
import Pagination from '@material-ui/lab/Pagination';
import { Link, Redirect } from 'react-router-dom';
const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: '#4C68FF',
		color: theme.palette.common.white,
	},
	body: {
		fontSize: 14,
	},
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: theme.palette.action.hover,
		},
	},
}))(TableRow);

function createData(username, question_title, Report_Count) {
	return { username, question_title, Report_Count };
}
function createDataForCheckList(username, question_title, answer_id, Report_Count) {
	return { username, question_title, answer_id, Report_Count };
}
const rows = [
	createData('Rofak', 'Does JSON data mean', 2),
	createData('Rofak', 'how it is used and what ', 3),
	createData('Rofak', 'context described later ', 1),
];
const rowCheckList = [
	createDataForCheckList('Rofak', 'Does JSON data mean', null, 2),
	createDataForCheckList('Rofak', null, 1, 3),
	createDataForCheckList('Rofak', 'context described later  ', null, 1),
];

const rowsAnswers = [createData('Rofak', 1, 3), createData('Rofak', 2, 1)];
const useStyles = makeStyles({
	table: {
		minWidth: 700,
	},
});
const page = makeStyles((theme) => ({
	root: {
		'& > *': {
			marginTop: theme.spacing(2),
		},
	},
}));
export default class ReportDetail extends Component {
	constructor(props) {
		super(props);
		this.state = {
			classes: {},
			person: [],
		};
	}
	componentWillMount() {
		this.setState({ classes: useStyles });
		Axios.get(`https://jsonplaceholder.typicode.com/users`).then((res) => {
			const persons = res.data;
			this.setState({ person: persons });
		});
	}
	render() {
		return (
			<div>
				{/* Report Question */}
				<ul className="ulHeader">
					<li className="subHeader">Report Question</li>
				</ul>
				<TableContainer component={Paper}>
					<Table className={this.state.classes.table} aria-label="customized table">
						<TableHead>
							<TableRow>
								<StyledTableCell align="center">Username</StyledTableCell>
								<StyledTableCell align="center">Question Title</StyledTableCell>
								<StyledTableCell align="center">Report Count</StyledTableCell>
								<StyledTableCell align="center">Action</StyledTableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{rows.map((row, index) => (
								<StyledTableRow key={index}>
									<StyledTableCell align="center">{row.username}</StyledTableCell>
									<StyledTableCell align="center">{row.question_title}</StyledTableCell>
									<StyledTableCell align="center">{row.Report_Count}</StyledTableCell>
									<StyledTableCell align="center">
										<Link to="/admin/report_question">
											<Button
												// href="/admin/report_detail"
												variant="contained"
												color="primary"
												style={{ marginLeft: '5px', backgroundColor: '#4C68FF' }}
											>
												View
											</Button>
										</Link>
									</StyledTableCell>
								</StyledTableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
				{/* Report Answers */}
				<ul className="ulHeader">
					<li className="subHeader">Report Answers</li>
				</ul>
				<TableContainer component={Paper}>
					<Table className={this.state.classes.table} aria-label="customized table">
						<TableHead>
							<TableRow>
								<StyledTableCell align="center">Username</StyledTableCell>
								<StyledTableCell align="center">Answer id</StyledTableCell>
								<StyledTableCell align="center">Report Count</StyledTableCell>
								<StyledTableCell align="center">Action</StyledTableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{rowsAnswers.map((row, index) => (
								<StyledTableRow key={index}>
									<StyledTableCell align="center">{row.username}</StyledTableCell>
									<StyledTableCell align="center">{row.question_title}</StyledTableCell>
									<StyledTableCell align="center">{row.Report_Count}</StyledTableCell>
									<StyledTableCell align="center">
										<Link to="/admin/report_answer">
											<Button
												// href="/admin/report_detail"
												variant="contained"
												color="primary"
												style={{ marginLeft: '5px', backgroundColor: '#4C68FF' }}
											>
												View
											</Button>
										</Link>
									</StyledTableCell>
								</StyledTableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
				{/* CheckList */}
				<ul className="ulHeader">
					<li className="subHeader" style={{ width: '80px' }}>
						CheckList
					</li>
				</ul>
				<TableContainer component={Paper}>
					<Table className={this.state.classes.table} aria-label="customized table">
						<TableHead>
							<TableRow>
								<StyledTableCell align="center">Username</StyledTableCell>
								<StyledTableCell align="center">Question Title</StyledTableCell>
								<StyledTableCell align="center">Answer id</StyledTableCell>
								<StyledTableCell align="center">Report Count</StyledTableCell>
								<StyledTableCell align="center">Action</StyledTableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{rowCheckList.map((row, index) => (
								<StyledTableRow key={index}>
									<StyledTableCell align="center">{row.username}</StyledTableCell>
									<StyledTableCell align="center">{row.question_title}</StyledTableCell>
									<StyledTableCell align="center">{row.answer_id}</StyledTableCell>
									<StyledTableCell align="center">{row.Report_Count}</StyledTableCell>
									<StyledTableCell align="center">
										{row.question_title != null ? (
											<Link to="/admin/report_question">
												{' '}
												<Button
													variant="contained"
													color="primary"
													style={{ marginLeft: '5px', backgroundColor: '#4C68FF' }}
												>
													View
												</Button>
											</Link>
										) : (
											<Link to="/admin/report_answer">
												<Button
													variant="contained"
													color="primary"
													style={{ marginLeft: '5px', backgroundColor: '#4C68FF' }}
												>
													View
												</Button>
											</Link>
										)}
									</StyledTableCell>
								</StyledTableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
				<div className={page.root} style={{ marginTop: '20px' }}>
					<Pagination
						count={10}
						shape="rounded"
						color="primary"
						style={{ width: '345px', margin: '0 auto' }}
					/>
				</div>
			</div>
		);
	}
}
