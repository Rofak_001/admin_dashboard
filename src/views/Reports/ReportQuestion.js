import React, { Component } from 'react';
import 'assets/css/ReportQuestonStyle.css';
import codePng from 'assets/img/code.PNG';
import Panha from 'assets/img/Panha.jpg';
import ViewPng from 'assets/img/view.png';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import TimePng from 'assets/img/Time.png';
import MorePng from 'assets/img/more.png';
//card
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
//drop down delete
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import CardReport from 'MyComponent/CardReport';
const useStyles = makeStyles((theme) => ({
	root: {
		flexGrow: 1,
	},
	paper: {
		padding: theme.spacing(2),
		textAlign: 'center',
		color: theme.palette.text.secondary,
	},
}));
//card style
const cardStyle = makeStyles({
	root: {
		minWidth: 275,
	},
	bullet: {
		display: 'inline-block',
		margin: '0 2px',
		transform: 'scale(0.8)',
	},
	title: {
		fontSize: 14,
	},
	pos: {
		marginBottom: 12,
	},
});
// style menu
const StyledMenu = withStyles({
	paper: {
		border: '1px solid #d3d4d5',
	},
})((props) => (
	<Menu
		elevation={0}
		getContentAnchorEl={null}
		anchorOrigin={{
			vertical: 'bottom',
			horizontal: 'center',
		}}
		transformOrigin={{
			vertical: 'top',
			horizontal: 'center',
		}}
		{...props}
	/>
));
//Sytle Menu Item
const StyledMenuItem = withStyles((theme) => ({
	root: {
		'&:focus': {
			backgroundColor: theme.palette.primary.main,
			'& .MuiListItemIcon-root, & .MuiListItemText-primary': {
				color: theme.palette.common.white,
			},
		},
	},
}))(MenuItem);
export default class ReportQuestion extends Component {
	constructor(props) {
		super(props);
		this.state = {
			anchorEl: null,
		};
	}
	handleClick = (event) => {
		this.setState({ anchorEl: event.currentTarget });
	};
	handleClose = () => {
		this.setState({ anchorEl: null });
	};
	render() {
		return (
			<div>
				<Box display="flex" m={1} p={1} style={{ margin: '0', padding: '0' }}>
					<Box flexGrow={1}>
						<h5 className="titleQu">Does JSON data mean dictionary in Flask?</h5>
					</Box>
					<Box>
						<Button
							aria-controls="customized-menu"
							aria-haspopup="true"
							onClick={this.handleClick.bind(this)}
						>
							<img src={MorePng} className="user" />
						</Button>

						<StyledMenu
							id="customized-menu"
							anchorEl={this.state.anchorEl}
							keepMounted
							open={Boolean(this.state.anchorEl)}
							onClose={this.handleClose.bind(this)}
						>
							<MenuList id="customized-menu">
								<MenuItem onClick={()=>alert("hello")}>Delete</MenuItem>
							</MenuList>
						</StyledMenu>
					</Box>
				</Box>
				<p className="content">
					This may be a silly question but I am really confused(I am a newbie). I am trying to make an API
					that accepts JSON as input and I am using Flask. The API takes POST method, so when a request comes
					along, it gets the JSON data from the body using
				</p>
				<div style={{ textAlign: 'center' }}>
					<img src={codePng} />
				</div>
				<div style={{ width: '100%', borderBottom: '2px solid black' }}>
					<Box display="flex" justifyContent="flex-start" m={1} p={1} style={{ margin: '0', padding: '0' }}>
						<Box p={1}>
							<img src={Panha} className="user" />
						</Box>
						<Box p={1} flexGrow={5}>
							<h6 className="nameuser">Long Panha</h6>
						</Box>
						<Box p={1}>
							<img src={ViewPng} className="user" />
						</Box>
						<Box p={1} flexGrow={5}>
							<h6 className="nameuser">៤៣​ មើល</h6>
						</Box>
						<Box p={1}>
							<img src={TimePng} className="user" />
						</Box>
						<Box p={1} flexGrow={15}>
							<h6 className="nameuser">មករា​ ១ ២០២០</h6>
						</Box>
					</Box>
				</div>
				<ul className="ulHeader">
					<li className="subHeader" style={{width:"75px"}}>Reporter</li>
				</ul>
				<div>
					<CardReport/>
					<CardReport/>
				</div>
			</div>
		);
	}
}
