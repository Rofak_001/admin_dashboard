import React, { Component } from 'react';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';
import { FormControl, Button, Box } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import NativeSelect from '@material-ui/core/NativeSelect';
import 'assets/css/CategoryStyle.css';
import InputBase from '@material-ui/core/InputBase';
// table
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Axios from 'axios';
import Pagination from '@material-ui/lab/Pagination';
const BootstrapInput = withStyles((theme) => ({
	root: {
		'label + &': {
			marginTop: theme.spacing(3),
		},
	},
	input: {
		borderRadius: 4,
		position: 'relative',
		backgroundColor: theme.palette.background.paper,
		border: '1px solid #ced4da',
		fontSize: 16,
		padding: '10px 26px 10px 12px',
		transition: theme.transitions.create(['border-color', 'box-shadow']),
		// Use the system font instead of the default Roboto font.
		fontFamily: [
			'-apple-system',
			'BlinkMacSystemFont',
			'"Segoe UI"',
			'Roboto',
			'"Helvetica Neue"',
			'Arial',
			'sans-serif',
			'"Apple Color Emoji"',
			'"Segoe UI Emoji"',
			'"Segoe UI Symbol"',
		].join(','),
		'&:focus': {
			borderRadius: 4,
			borderColor: '#80bdff',
			boxShadow: '0 0 0 0.2rem rgba(0,123,255,.25)',
		},
	},
}))(InputBase);
//table
const styte = makeStyles({
	palette: {
		secondary: {
			main: '#00c853',
		},
	},
});
const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: '#4C68FF',
		color: theme.palette.common.white,
	},
	body: {
		fontSize: 14,
	},
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: theme.palette.action.hover,
		},
	},
}))(TableRow);

function createData(id, CateogryName) {
	return { id, CateogryName };
}

function createDataForTag(id, TagName, CateogryName) {
	return { id, TagName, CateogryName };
}

const rows = [createData(1, 'Web'), createData(2, 'Android'), createData(3, 'Reactjs'), createData(4, 'IOS')];
const rowsTag = [
	createDataForTag(1, 'HTML', 'Web'),
	createDataForTag(2, 'java', 'Android'),
	createDataForTag(3, 'javaScript', 'Reactjs'),
	createDataForTag(4, 'Swit', 'IOS'),
];
const useStyles = makeStyles({
	table: {
		minWidth: 700,
	},
});
const page = makeStyles((theme) => ({
	root: {
		'& > *': {
			marginTop: theme.spacing(2),
		},
	},
}));
export default class Cateogries extends Component {
	constructor(props) {
		super(props);
		this.state = {
			age: '',
			classes: {},
		};
	}
	componentWillMount() {
		this.setState({ classes: useStyles });
	}
	handleChange = (event) => {
		alert(event.target.value);
		this.setState({ age: event.target.value });
	};
	render() {
		return (
			<div>
				<Box display="flex" flexDirection="row" p={1} m={1}>
					<Box>
						<FormControl>
							<FormLabel style={{ marginBottom: '15px' }}>Add Category</FormLabel>
							<TextField
								id="outlined-basic"
								label="Cateogry"
								variant="outlined"
								size="small"
								style={{ marginBottom: '20px', backgroundColor: '#FFFFFF' }}
							/>
						</FormControl>
					</Box>
					<Box alignSelf="center" className="al">
						<Button variant="contained" color="primary">
							Add
						</Button>
					</Box>
				</Box>
				<Box display="flex" flexDirection="row" p={1} m={1}>
					<Box>
						<FormControl>
							<FormLabel style={{ marginBottom: '15px' }}>Add Tag</FormLabel>
							<TextField
								id="outlined-basic"
								label="Tag"
								variant="outlined"
								size="small"
								style={{ marginBottom: '20px', backgroundColor: '#FFFFFF' }}
							/>
						</FormControl>
					</Box>
					<Box alignSelf="center" className="op">
						<FormControl>
							<NativeSelect
								id="demo-customized-select-native"
								value={this.state.age}
								onChange={this.handleChange.bind(this)}
								input={<BootstrapInput />}
								className="op wOp"
							>
								<option aria-label="None" value="" />
								<option value={10}>Android</option>
								<option value={20}>Web</option>
								<option value={30}>Reactjs</option>
							</NativeSelect>
						</FormControl>
					</Box>
					<Box alignSelf="center" className="al">
						<Button variant="contained" color="primary">
							Add
						</Button>
					</Box>
				</Box>
				<div>
					{/* Table Cateory */}
					<Box display="flex" flexDirection="row" p={1} m={1}>
						<Box className="mTbCategory">
							<TableContainer component={Paper}>
								<Table className={this.state.classes.table} aria-label="customized table">
									<TableHead>
										<TableRow>
											<StyledTableCell>Id</StyledTableCell>
											<StyledTableCell align="center">Cateogry Name</StyledTableCell>
											<StyledTableCell align="center">Action</StyledTableCell>
										</TableRow>
									</TableHead>
									<TableBody>
										{rows.map((row) => (
											<StyledTableRow key={row.id}>
												<StyledTableCell component="th" scope="row">
													{row.id}
												</StyledTableCell>
												<StyledTableCell align="center">{row.CateogryName}</StyledTableCell>
												<StyledTableCell align="center">
													<Button
														variant="contained"
														style={{backgroundColor: '#4C68FF',color:"white"}}	
													>
														Update
													</Button>
													<Button
														variant="contained"
														color="secondary"
														style={{ marginLeft: '5px'}}
														onClick={() => alert(row.id)}
													>
														Delete
													</Button>
												</StyledTableCell>
											</StyledTableRow>
										))}
									</TableBody>
								</Table>
							</TableContainer>
						</Box>
						<Box flexGrow={3}>
							<TableContainer component={Paper}>
								<Table className={this.state.classes.table} aria-label="customized table">
									<TableHead>
										<TableRow>
											<StyledTableCell>Id</StyledTableCell>
											<StyledTableCell>Tag Name</StyledTableCell>
											<StyledTableCell align="center">Cateogry Name</StyledTableCell>
											<StyledTableCell align="center">Action</StyledTableCell>
										</TableRow>
									</TableHead>
									<TableBody>
										{rowsTag.map((row) => (
											<StyledTableRow key={row.id}>
												<StyledTableCell component="th" scope="row">
													{row.id}
												</StyledTableCell>
												<StyledTableCell align="center">{row.TagName}</StyledTableCell>
												<StyledTableCell align="center">{row.CateogryName}</StyledTableCell>
												<StyledTableCell align="center">
													<Button
														variant="contained"
														style={{backgroundColor: '#4C68FF',color:"white"}}													
													>
														Update
													</Button>
													<Button
														variant="contained"
														color="secondary"
														style={{ marginLeft: '5px'}}
														onClick={() => alert(row.id)}
													>
														Delete
													</Button>
												</StyledTableCell>
											</StyledTableRow>
										))}
									</TableBody>
								</Table>
							</TableContainer>
							<div className={page.root} style={{ marginTop: '20px' }}>
								<Pagination
									count={10}
									shape="rounded"
									color="primary"
									style={{ width: '345px', margin: '0 auto' }}
								/>
							</div>
						</Box>
					</Box>
				</div>
			</div>
		);
	}
}
