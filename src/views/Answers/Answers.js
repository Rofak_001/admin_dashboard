/*eslint-disable*/
// import React from 'react';
import React, { Component } from 'react';
// @material-ui/core components
// import { makeStyles } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';
// core components
import GridItem from 'components/Grid/GridItem.js';
import GridContainer from 'components/Grid/GridContainer.js';
import Card from 'components/Card/Card.js';
import CardHeader from 'components/Card/CardHeader.js';
import CardBody from 'components/Card/CardBody.js';

import styles from 'assets/jss/material-dashboard-react/views/iconsStyle.js';
//table
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Axios from 'axios';
import Pagination from '@material-ui/lab/Pagination';
const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: '#4C68FF',
		color: theme.palette.common.white,
	},
	body: {
		fontSize: 14,
	},
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: theme.palette.action.hover,
		},
	},
}))(TableRow);

function createData(id, username, question_id, answer_date, isAccepted, status, vote_count, answer_id) {
	return { id, username, question_id, answer_date, isAccepted, status, vote_count, answer_id };
}

const rows = [
	createData(1, 'Panha', '1', '20/1/2020', 'true', 'Active', 21, 1),
	createData(2, 'Kok', '3', '20/12/2020', 'true', 'Disabled', 12, 4),
	createData(3, 'Panha', '5', '20/11/2020', 'false', 'Active', 3, 2),
	createData(4, 'Kok', '1', '3/1/2020', 'true', 'Disabled', 12, 4),
	createData(5, 'Panha', '2', '20/1/2020', 'true', 'Disabled', 43, 5),
	createData(6, 'Kok', '3', '20/1/2020', 'false', 'Active', 23, 6),
];

const useStyles = makeStyles({
	table: {
		minWidth: 700,
	},
});
const page = makeStyles((theme) => ({
	root: {
		'& > *': {
			marginTop: theme.spacing(2),
		},
	},
}));

export default class Answers extends Component {
	constructor(props) {
		super(props);
		this.state = {
			classes: {},
			person: [],
		};
	}
	componentWillMount() {
		this.setState({ classes: useStyles });
		Axios.get(`https://jsonplaceholder.typicode.com/users`).then((res) => {
			const persons = res.data;
			this.setState({ person: persons });
		});
	}
	render() {
		return (
			<div>
				<TextField
					id="outlined-basic"
					label="Search"
					variant="outlined"
					size="small"
					style={{ marginBottom: '20px', backgroundColor: '#FFFFFF' }}
				/>
				<TableContainer component={Paper}>
					<Table className={this.state.classes.table} aria-label="customized table">
						<TableHead>
							<TableRow>
								<StyledTableCell>Id</StyledTableCell>
								<StyledTableCell align="center">Username</StyledTableCell>
								<StyledTableCell align="center">Question Id</StyledTableCell>
								<StyledTableCell align="center">Answer date</StyledTableCell>
								<StyledTableCell align="center">IsAccepted</StyledTableCell>
								<StyledTableCell align="center">Status</StyledTableCell>
								<StyledTableCell align="center">Vote Count</StyledTableCell>
								<StyledTableCell align="center">Answer Id</StyledTableCell>
								<StyledTableCell align="center">Action</StyledTableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{rows.map((row) => (
								<StyledTableRow key={row.id}>
									<StyledTableCell component="th" scope="row">
										{row.id}
									</StyledTableCell>
									<StyledTableCell align="center">{row.username}</StyledTableCell>
									<StyledTableCell align="center">{row.question_id}</StyledTableCell>
									<StyledTableCell align="center">{row.answer_date}</StyledTableCell>
									<StyledTableCell align="center">{row.isAccepted}</StyledTableCell>
									<StyledTableCell align="center">{row.status}</StyledTableCell>
									<StyledTableCell align="center">{row.vote_count}</StyledTableCell>
									<StyledTableCell align="center">{row.answer_id}</StyledTableCell>
									<StyledTableCell align="center">
										{row.status == 'Active' ? (
											<Button variant="contained" color="secondary">
												Disable
											</Button>
										) : (
											<Button variant="contained" style={{backgroundColor:"#00c853",color:"white"}}>
												Active
											</Button>
										)}
										<Button
											variant="contained"
											color="primary"
											style={{ marginLeft: '5px', backgroundColor: '#4C68FF' }}
											onClick={() => alert(row.id)}
										>
											View
										</Button>
									</StyledTableCell>
								</StyledTableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
				<div className={page.root} style={{ marginTop: '20px' }}>
					<Pagination
						count={10}
						shape="rounded"
						color="primary"
						style={{ width: '345px', margin: '0 auto' }}
					/>
				</div>
			</div>
		);
	}
}
