import React, { Component } from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Axios from 'axios';
import { Link, Redirect } from 'react-router-dom';
import Pagination from '@material-ui/lab/Pagination';
const StyledTableCell = withStyles((theme) => ({
	head: {
		backgroundColor: '#4C68FF',
		color: theme.palette.common.white,
	},
	body: {
		fontSize: 14,
	},
}))(TableCell);
const StyledTableRow = withStyles((theme) => ({
	root: {
		'&:nth-of-type(odd)': {
			backgroundColor: theme.palette.action.hover,
		},
	},
}))(TableRow);
function createData(id, username, gender, address, Work_Place, School,Status) {
	return { id, username, gender, address, Work_Place, School,Status };
}
const rows = [
  createData(1, 'Panha', 'Male', 'Phnom Penh', 'Phnom Penh', 'RUPP','Active'),
  createData(2, 'Rofak', 'Male', 'Phnom Penh', 'Phnom Penh', 'RUPP','Active'),
  createData(3, 'Nazimah', 'Female', 'Phnom Penh', 'Phnom Penh', 'RUPP','Disable'),
  createData(4, 'LepHang', 'Male', 'Phnom Penh', 'Phnom Penh', 'RUPP','Active'),
  createData(5, 'Khetbung', 'Male', 'Phnom Penh', 'Phnom Penh', 'RUPP','Disable'),

];
const useStyles = makeStyles({
	table: {
		minWidth: 700,
	},
});
const page = makeStyles((theme) => ({
	root: {
		'& > *': {
			marginTop: theme.spacing(2),
		},
	},
}));
export default class TableUser extends Component {
	constructor(props) {
		super(props);
		this.state = {
			classes: {},
			person: [],
		};
	}
	componentWillMount() {
		this.setState({ classes: useStyles });
		Axios.get(`https://jsonplaceholder.typicode.com/users`).then((res) => {
			const persons = res.data;
			this.setState({ person: persons });
		});
	}
	render() {
		return (
			<div>
				<TextField
					id="outlined-basic"
					label="Search"
					variant="outlined"
					size="small"
					style={{ marginBottom: '20px', backgroundColor: '#FFFFFF' }}
				/>
				<TableContainer component={Paper}>
					<Table className={this.state.classes.table} aria-label="customized table">
						<TableHead>
							<TableRow>
								<StyledTableCell align="center">Id</StyledTableCell>
								<StyledTableCell align="center">Username</StyledTableCell>
								<StyledTableCell align="center">Gender</StyledTableCell>
								<StyledTableCell align="center">Address</StyledTableCell>
                <StyledTableCell align="center">Work Place</StyledTableCell>
                <StyledTableCell align="center">School</StyledTableCell>
                <StyledTableCell align="center">Status</StyledTableCell>
                <StyledTableCell align="center">Action</StyledTableCell>
							</TableRow>
						</TableHead>
						<TableBody>
							{rows.map((row) => (
								<StyledTableRow key={row.id}>
                  <StyledTableCell align="center">{row.id}</StyledTableCell>
									<StyledTableCell align="center">{row.username}</StyledTableCell>
									<StyledTableCell align="center">{row.gender}</StyledTableCell>
									<StyledTableCell align="center">{row.address}</StyledTableCell>
                  <StyledTableCell align="center">{row.Work_Place}</StyledTableCell>
                  <StyledTableCell align="center">{row.School}</StyledTableCell>
                  <StyledTableCell align="center">{row.Status}</StyledTableCell>
									<StyledTableCell align="center">
                      {row.Status=="Active"?<Button
											// href="/admin/report_detail"
											variant="contained"
											color="secondary"
											style={{ marginLeft: '5px'}}
										>
                      Disable
										</Button>:<Button
											// href="/admin/report_detail"
											variant="contained"
											color="primary"
											style={{ marginLeft: '5px'}}
										>
                      Enable
										</Button>}
									</StyledTableCell>
								</StyledTableRow>
							))}
						</TableBody>
					</Table>
				</TableContainer>
				<div className={page.root} style={{ marginTop: '20px' }}>
					<Pagination
						count={10}
						shape="rounded"
						color="primary"
						style={{ width: '345px', margin: '0 auto' }}
					/>
				</div>
			</div>
		);
	}
}
