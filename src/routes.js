/*!

=========================================================
* Material Dashboard React - v1.9.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.js";
import TableUser from "views/TableUser/TableUser.js";
import Categories from "views/Category/Cateogries";
import Question from "views/Question/Questions.js";
import Answers from "views/Answers/Answers.js";
import Reports from "views/Reports/Reports.js";
// core components/views for RTL layout
// Icon for sidebar
import ReportIcon from '@material-ui/icons/Report';
import ContactSupportIcon from '@material-ui/icons/ContactSupport';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';
import ReportDetail from "views/Reports/ReportDetail";
const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon:Dashboard,
    component: DashboardPage,
    layout: "/admin"
  },
  {
    path: "/tableUser",
    name: "Table Users",
    icon: Person,
    component: TableUser,
    layout: "/admin"
  },
  {
    path: "/category",
    name: "Categories",
    icon: "content_paste",
    component: Categories,
    layout: "/admin"
  },
  {
    path: "/question",
    name: "Questions",
    icon: ContactSupportIcon,
    component: Question,
    layout: "/admin"
  },
  {
    path: "/answers",
    name: "Answers",
    icon: QuestionAnswerIcon,
    component: Answers,
    layout: "/admin"
  },
  {
    path: "/reports",
    name: "Reports",
    icon: ReportIcon,
    component: Reports,
    layout: "/admin"
  },
];

export default dashboardRoutes;
